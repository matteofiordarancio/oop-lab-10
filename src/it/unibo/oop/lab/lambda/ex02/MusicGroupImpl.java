package it.unibo.oop.lab.lambda.ex02;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return songs.stream().map(x -> x.getSongName()).sorted();
    }

    @Override
    public Stream<String> albumNames() {
        return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        return albums.entrySet().stream()
                .filter(x -> x.getValue() == year)
                .map(y -> y.getKey());
    }

    @Override
    public int countSongs(final String albumName) {
        return songs.stream().filter(x -> x.getAlbumName().orElse("not present").equals(albumName)).mapToInt(x -> 1).sum();
    }

    @Override
    public int countSongsInNoAlbum() {
        return songs.stream().filter(x -> !x.getAlbumName().isPresent()).mapToInt(x -> 1).sum();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        return songs.stream().filter(x -> x.getAlbumName().orElse("notFound").equals(albumName)).mapToDouble(x -> x.getDuration())
                .average();
    }

    @Override
    public Optional<String> longestSong() {
        //        return Optional.of(songs.stream()
        //                .filter(y -> y.getDuration() == songs.stream().mapToDouble(x -> x.getDuration()).max().orElse(-1))
        //                .findFirst()
        // .get().getSongName());
        return songs.stream().max((x, y) -> Double.compare(x.getDuration(), y.getDuration())).map(Song::getSongName);
    }

    @Override
    public Optional<String> longestAlbum() {
        // double temp, max;
        // Optional<String> target = Optional.empty();
        // max = -1;
        // for (final String albumName : albums.keySet()) {
        // temp = songs.stream().filter(y -> y.getAlbumName().orElse("no
        // Album").equals(albumName))
        // .mapToDouble(x -> x.getDuration()).sum();
        // if (temp > max) {
        // max = temp;
        // target = Optional.of(albumName);
        // }
        // }
        // return target;
        return albums.entrySet().stream()
                .map(kv -> new AbstractMap.SimpleEntry<>(kv.getKey(), getAlbumDuration(kv.getKey())))
                .max((x, y) -> Double.compare(x.getValue(), y.getValue())).map(Entry::getKey);
    }

    /**
     * Does nothing.
     * @param albumName a.
     * @return A.
     */
    public double getAlbumDuration(final String albumName) {
        return songs.stream().filter(y -> y.getAlbumName().orElse("no Album").equals(albumName))
                .mapToDouble(x -> x.getDuration()).sum();
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            songName = name;
            albumName = album;
            duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
